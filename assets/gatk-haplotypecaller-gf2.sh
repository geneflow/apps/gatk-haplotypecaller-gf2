#!/bin/bash

# GATK HaplotypeCaller wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input => BAM File and Index Directory"
    echo "  --reference_sequence => Reference Sequence Directory"
    echo "  --sample_ploidy => Sample Ploidy"
    echo "  --emit_reference_confidence => Reference Confidence Mode"
    echo "  --pair_hmm_threads => Native PairHMM Threads"
    echo "  --output => Output Directory"
    echo "  --exec_method => Execution method (cdc-shared-singularity, singularity, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input:,reference_sequence:,sample_ploidy:,emit_reference_confidence:,pair_hmm_threads:,output:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
SAMPLE_PLOIDY="2"
EMIT_REFERENCE_CONFIDENCE="NONE"
PAIR_HMM_THREADS="4"
EXEC_METHOD="auto"
EXEC_INIT=""
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input)
            if [ -z "${input}" ]; then
                INPUT=$2
            else
                INPUT=${input}
            fi
            shift 2
            ;;
        --reference_sequence)
            if [ -z "${reference_sequence}" ]; then
                REFERENCE_SEQUENCE=$2
            else
                REFERENCE_SEQUENCE=${reference_sequence}
            fi
            shift 2
            ;;
        --sample_ploidy)
            if [ -z "${sample_ploidy}" ]; then
                SAMPLE_PLOIDY=$2
            else
                SAMPLE_PLOIDY=${sample_ploidy}
            fi
            shift 2
            ;;
        --emit_reference_confidence)
            if [ -z "${emit_reference_confidence}" ]; then
                EMIT_REFERENCE_CONFIDENCE=$2
            else
                EMIT_REFERENCE_CONFIDENCE=${emit_reference_confidence}
            fi
            shift 2
            ;;
        --pair_hmm_threads)
            if [ -z "${pair_hmm_threads}" ]; then
                PAIR_HMM_THREADS=$2
            else
                PAIR_HMM_THREADS=${pair_hmm_threads}
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT=${output}
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD=${exec_method}
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT=${exec_init}
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input: ${INPUT}"
echo "Reference_sequence: ${REFERENCE_SEQUENCE}"
echo "Sample_ploidy: ${SAMPLE_PLOIDY}"
echo "Emit_reference_confidence: ${EMIT_REFERENCE_CONFIDENCE}"
echo "Pair_hmm_threads: ${PAIR_HMM_THREADS}"
echo "Output: ${OUTPUT}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT input

if [ -z "${INPUT}" ]; then
    echo "BAM File and Index Directory required"
    echo
    usage
    exit 1
fi
# make sure INPUT is staged
count=0
while [ ! -d "${INPUT}" ]
do
    echo "${INPUT} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -d "${INPUT}" ]; then
    echo "BAM File and Index Directory not found: ${INPUT}"
    exit 1
fi
INPUT_FULL=$(readlink -f "${INPUT}")
INPUT_DIR=$(dirname "${INPUT_FULL}")
INPUT_BASE=$(basename "${INPUT_FULL}")


# REFERENCE_SEQUENCE input

if [ -z "${REFERENCE_SEQUENCE}" ]; then
    echo "Reference Sequence Directory required"
    echo
    usage
    exit 1
fi
# make sure REFERENCE_SEQUENCE is staged
count=0
while [ ! -d "${REFERENCE_SEQUENCE}" ]
do
    echo "${REFERENCE_SEQUENCE} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -d "${REFERENCE_SEQUENCE}" ]; then
    echo "Reference Sequence Directory not found: ${REFERENCE_SEQUENCE}"
    exit 1
fi
REFERENCE_SEQUENCE_FULL=$(readlink -f "${REFERENCE_SEQUENCE}")
REFERENCE_SEQUENCE_DIR=$(dirname "${REFERENCE_SEQUENCE_FULL}")
REFERENCE_SEQUENCE_BASE=$(basename "${REFERENCE_SEQUENCE_FULL}")



# SAMPLE_PLOIDY parameter
if [ -n "${SAMPLE_PLOIDY}" ]; then
    :
else
    :
fi


# EMIT_REFERENCE_CONFIDENCE parameter
if [ -n "${EMIT_REFERENCE_CONFIDENCE}" ]; then
    :
else
    :
fi


# PAIR_HMM_THREADS parameter
if [ -n "${PAIR_HMM_THREADS}" ]; then
    :
else
    :
fi


# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output Directory required"
    echo
    usage
    exit 1
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="cdc-shared-singularity singularity auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if command -v singularity >/dev/null 2>&1 && [ -f "/apps/standalone/singularity/gatk/gatk-4.1.4.1-biocontainers.simg" ]; then
        AUTO_EXEC=cdc-shared-singularity
    elif command -v singularity >/dev/null 2>&1; then
        AUTO_EXEC=singularity
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${OUTPUT_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
MNT=""; ARG=""; CMD0="mkdir -p ${LOG_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    cdc-shared-singularity)
        MNT=""; ARG=""; ARG="${ARG} --input"; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_DIR}:/data1\""; ARG="${ARG} \"/data1/${INPUT_BASE}/${INPUT_BASE}.bam\""; ARG="${ARG} --sample-ploidy"; ARG="${ARG} \"${SAMPLE_PLOIDY}\""; ARG="${ARG} --emit-ref-confidence"; ARG="${ARG} \"${EMIT_REFERENCE_CONFIDENCE}\""; ARG="${ARG} --native-pair-hmm-threads"; ARG="${ARG} \"${PAIR_HMM_THREADS}\""; ARG="${ARG} --reference"; MNT="${MNT} -B "; MNT="${MNT}\"${REFERENCE_SEQUENCE_DIR}:/data5\""; ARG="${ARG} \"/data5/${REFERENCE_SEQUENCE_BASE}/${REFERENCE_SEQUENCE_BASE}.fasta\""; if [ "${EMIT_REFERENCE_CONFIDENCE}" != "GVCF" ]; then ARG="${ARG} --output"; MNT="${MNT} -B "; MNT="${MNT}\"${OUTPUT_DIR}:/data6\""; ARG="${ARG} \"/data6/${OUTPUT_BASE}/${OUTPUT_BASE}.vcf\""; fi; if [ "${EMIT_REFERENCE_CONFIDENCE}" = "GVCF" ]; then ARG="${ARG} --output"; MNT="${MNT} -B "; MNT="${MNT}\"${OUTPUT_DIR}:/data7\""; ARG="${ARG} \"/data7/${OUTPUT_BASE}/${OUTPUT_BASE}.g.vcf\""; fi; CMD0="singularity -s exec ${MNT} /apps/standalone/singularity/gatk/gatk-4.1.4.1-biocontainers.simg gatk HaplotypeCaller ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/${OUTPUT_BASE}-gatk-haplotypecaller.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/${OUTPUT_BASE}-gatk-haplotypecaller.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
    singularity)
        MNT=""; ARG=""; ARG="${ARG} --input"; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_DIR}:/data1\""; ARG="${ARG} \"/data1/${INPUT_BASE}/${INPUT_BASE}.bam\""; ARG="${ARG} --reference"; MNT="${MNT} -B "; MNT="${MNT}\"${REFERENCE_SEQUENCE_DIR}:/data2\""; ARG="${ARG} \"/data2/${REFERENCE_SEQUENCE_BASE}/${REFERENCE_SEQUENCE_BASE}.fasta\""; ARG="${ARG} --sample-ploidy"; ARG="${ARG} \"${SAMPLE_PLOIDY}\""; ARG="${ARG} --emit-ref-confidence"; ARG="${ARG} \"${EMIT_REFERENCE_CONFIDENCE}\""; ARG="${ARG} --native-pair-hmm-threads"; ARG="${ARG} \"${PAIR_HMM_THREADS}\""; if [ "${EMIT_REFERENCE_CONFIDENCE}" != "GVCF" ]; then ARG="${ARG} --output"; MNT="${MNT} -B "; MNT="${MNT}\"${OUTPUT_DIR}:/data6\""; ARG="${ARG} \"/data6/${OUTPUT_BASE}/${OUTPUT_BASE}.vcf\""; fi; if [ "${EMIT_REFERENCE_CONFIDENCE}" = "GVCF" ]; then ARG="${ARG} --output"; MNT="${MNT} -B "; MNT="${MNT}\"${OUTPUT_DIR}:/data7\""; ARG="${ARG} \"/data7/${OUTPUT_BASE}/${OUTPUT_BASE}.g.vcf\""; fi; CMD0="singularity -s exec ${MNT} docker://quay.io/biocontainers/gatk4:4.1.4.1--1 gatk HaplotypeCaller ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/${OUTPUT_BASE}-gatk-haplotypecaller.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/${OUTPUT_BASE}-gatk-haplotypecaller.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

