GATK HaplotypeCaller GeneFlow App
=================================

Version: 4.1.4.0-01

This GeneFlow app wraps the GATK HaplotypeCaller tool.

Inputs
------

1. input: Directory that contains BAM and BAI files.

2. reference_sequence: Directory that contains reference sequence fasta, index, and dict files.

Parameters
----------

1. sample_ploidy: Sample ploidy - ploidy of the sample. Default: 2.

2. emit_reference_confidence: Reference confidence score mode (NONE, BP_RESOLUTION, GVCF). Default: NONE.

3. pair_hmm_threads: Native PairHMM Threads - Number of threads for native pair HMM

4. output: Output directory - The name of the output directory to place VCF and VCF.idx files. Default: output.

